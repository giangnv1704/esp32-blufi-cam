/** @file: vsm_debug.c
 *  @desc: Debugging
 *  @author: v.manhbt1@vinsmart.net.vn
 *  HISTORY:
 *  Ver   Who       Date                Changes
 *  ----- --------- ------------------  ----------------------------------------
 *  0.0   ManhBT1                       Create
 *  0.1   DuyNH8   Nov 29, 2019         Modify comment
 *
 * *************************************************
    * Copyright (c) 2019 VinSmart R&M, JSC.
    * All Rights Reserved.
    * VinSmart R&M Confidential and Proprietary.
*/

#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <ctype.h>
#include "app_debug.h"



/**
 * @brief      Do nothing
 *
 * @param[in]  sz         The size
 * @param[in]  <unnamed>  { parameter_description }
 */
void debug_nothing(const char* sz, ...){

}






