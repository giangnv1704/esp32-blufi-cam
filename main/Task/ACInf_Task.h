/*
 * ACInf_Task.h
 *
 *  Created on: Mar 7, 2019
 *      Author: thaonm1
 */

#ifndef MAIN_TASK_ACINF_TASK_H_
#define MAIN_TASK_ACINF_TASK_H_

void ACInf_Task(void *pvParameters);

#endif /* MAIN_TASK_ACINF_TASK_H_ */
