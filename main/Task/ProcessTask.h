/*
 * ProcessTask.h
 *
 *  Created on: Sep 18, 2020
 *      Author: admin
 */

#ifndef MAIN_TASK_PROCESSTASK_H_
#define MAIN_TASK_PROCESSTASK_H_

#include <stdint.h>

void ProcessTask_Init(void);

void ProcessTask_SetEvent(uint32_t event);

#endif /* MAIN_TASK_PROCESSTASK_H_ */
